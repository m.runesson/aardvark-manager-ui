FROM docker-images.jobtechdev.se/mirror/debian:11-slim AS build-env

# Install flutter dependencies
RUN apt-get update
RUN apt-get install -y xz-utils curl git
RUN apt-get clean

WORKDIR /

# Clone the flutter repo
RUN curl https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_3.0.5-stable.tar.xz -o flutter.tar.xz
RUN tar xf flutter.tar.xz
RUN rm flutter.tar.xz

# Set flutter path
# RUN /usr/local/flutter/bin/flutter doctor -v
ENV PATH="/flutter/bin:/flutter/bin/cache/dart-sdk/bin:${PATH}"

# Pre populate dev binaries
RUN flutter precache
# Enable flutter web
#RUN flutter channel master
# RUN flutter upgrade
RUN flutter config --enable-web

# Copy files to container and build
RUN mkdir /app/
COPY . /app/
WORKDIR /app/
RUN flutter build web

# Stage 2 - Create the run-time image
FROM docker.io/library/nginx:1.23.0-alpine
COPY --from=build-env /app/build/web /usr/share/nginx/html