import 'dart:convert';

import 'package:aardvark_manager/Environment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

import 'model/Environments.dart';
import 'model/Project.dart';

class CreateProjectForm extends StatefulWidget {
  const CreateProjectForm({super.key});

  @override
  CreateProjectFormState createState() {
    return CreateProjectFormState();
  }
}

class CreateProjectFormState extends State<CreateProjectForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          const Text("Provide path to the project in GitLab that you want to take advantage of Aardvark"),
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty ) {
                return 'Provide a path to your project, example "arbetsformedlingen/devops/aardvark-demo"';
              }
              return null;
            }, // validator
          ),
          ElevatedButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                // If the form is valid, display a snackbar. In the real world,
                // you'd often call a server or save the information in a database.
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('Processing Data')),
                );
              }
            },
            child: const Text('Submit'),
          ),
        ],
      ),
    );
  }
}

Future<Project> createProject(String path, Map<String, bool> environments, String ownerTeam) async {
  if (path.startsWith("/")) {
    path = path.substring(1);
  }

  final envList = (environments..removeWhere((key, value) => value == false)).keys.toList();

  final response = await http.post(

    Uri.parse('${Environment.apiUrl}/api/v1/project'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json',
    },
    body: jsonEncode(<String, Object>{
      'project': path,
      'owner_team': ownerTeam,
      'environments': envList,
    }),
  );

  final jsonMap = jsonDecode(response.body);
  if (response.statusCode == 201 || response.statusCode == 200) {
    return Project.fromJson(jsonMap);
  } else {
    throw Exception('Failed to create project. ${jsonMap['message']}');
  }
}

Future<Environments> createEnvironments() async {
  final response = await http.get(
    Uri.parse('${Environment.apiUrl}/api/v1/environments'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json',
    },
  );

  final jsonMap = jsonDecode(response.body);
  if (response.statusCode == 201 || response.statusCode == 200) {
    return Environments.fromJson(jsonMap);
  } else {
    throw Exception('Cannot get environments.');
  }
}