
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:aardvark_manager/Environment.dart';


class OptionsModel extends ChangeNotifier {
  List<String>  environments = [];
  List<String>  teams = [];


  void fetchEnvironments() async {
    final response = await http.get(
      Uri.parse('${Environment.apiUrl}/api/v1/environments'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
      },
    );

    if (response.statusCode == 201 || response.statusCode == 200) {
      final jsonMap = jsonDecode(response.body);
      final envData = jsonMap['environments'] as List<dynamic>?;
      environments = envData != null ? envData.map((env) => env['name'] as String).toList() : <String>[];
      environments.sort((a,b) => a.compareTo(b));
      notifyListeners();
    } else {
      throw Exception('Cannot get environments.');
    }
  }

  void fetchTeams() async {
    final response = await http.get(
      Uri.parse('${Environment.apiUrl}/api/v1/allowedowners'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Accept': 'application/json',
      },
    );

    if (response.statusCode == 201 || response.statusCode == 200) {
      final jsonMap = jsonDecode(response.body);
      final groupsData = jsonMap['groups'] as List<dynamic>?;
      teams = groupsData != null ? groupsData.map((group) => group['name'] as String).toList() : <String>[];
      teams.sort((a,b) => a.compareTo(b));
      notifyListeners();
    } else {
      throw Exception('Cannot get Teams.');
    }
  }
}