class Project {
  final GitLabProject srcProject;
  final GitLabProject infraProject;
  final String        mergeRequestNamespaces;
  final String        mergeRequestArgocd;

  const Project({
    required this.srcProject,
    required this.infraProject,
    required this.mergeRequestNamespaces,
    required this.mergeRequestArgocd});

  factory Project.fromJson(Map<String, dynamic> json) {
    return Project(
      srcProject: GitLabProject.fromJson(json['src_project']),
      infraProject: GitLabProject.fromJson(json['infra_project']),
      mergeRequestNamespaces: json['merge_request_namespaces'],
      mergeRequestArgocd: json['merge_request_argocd'],
    );
  }
}

class GitLabProject {
  final int id;
  final int namespaceId;
  final String name;
  final String path;
  final String httpRepoUrl;
  final String defaultBranch;
  
  const GitLabProject({
  required this.id,
  required this.namespaceId,
  required this.name,
  required this.path,
  required this.httpRepoUrl,
  required this.defaultBranch});
  
  factory GitLabProject.fromJson(Map<String, dynamic> json) {
    return GitLabProject(
      id: json['project_id'],
      namespaceId: json['namespace_id'],
      name: json['name'],
      path: json['path'],
      httpRepoUrl: json['http_repo_url'],
      defaultBranch: json['default_branch'],
    );
  }
}
